{
  description = "An overlay for EVT use";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-utils }: {
    overlay = import ./overlay.nix;
  };
}
