{ lib, stdenv, fetchFromGitHub, pkg-config, cmake
, eigen
, opencv
, wxGTK31
, wxGTK31-gtk3
, libsForQt5
, libLAS
, libpcap
, suitesparse
, zlib
, libjpeg
, octomap
, nanoflann
, assimp }:

stdenv.mkDerivation rec {
  pname = "mrpt";
  version = "2.4.3";

  src = fetchFromGitHub {
    owner = "MRPT";
    repo = "mrpt";
    rev = version;
    fetchSubmodules = true;
    sha256 = "0jf95m95vqvm9w1xqyagh79bgx80h1jfvq05rpjsj6hk8bj2s11r";
  };

  nativeBuildInputs = [
    pkg-config
    cmake
  ];

  buildInputs = [
    eigen
    opencv
    wxGTK31
    wxGTK31-gtk3
    libsForQt5.full
    libLAS
    libpcap
    suitesparse
    zlib

    libjpeg
    octomap
    nanoflann
    assimp
  ];

  configurePhase = ''
    mkdir build
    cd build
    cmake ..
  '';

  buildPhase = ''
    cmake --build . -j $NIX_BUILD_CORES
  '';

  installPhase = ''
    cmake --install . --prefix $out
  '';
}
